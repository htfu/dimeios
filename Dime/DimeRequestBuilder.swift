//
//  DimeRequestBuilder.swift
//  Dime
//
//  Created by Nishant Jain on 3/30/16.
//  Copyright © 2016 Dime. All rights reserved.
//

import Foundation
import Alamofire

class DimeRequestBuilder {
    
    static func requestLogin(GroupName : String) -> Bool {
        print("Requesting Login with Group Name: \(GroupName)")
        Utility.setAlphaCodeFromPreferences(GroupName)
        print("Send network request")
        
        return true
    }
    
    static func requestTopicList() -> [Topic] {
        print("Send HTTP request for Topics")
        
        //replace with actualy network requests with protocols/interfaces
        return Utility.getDummyQuestions()
    }
    
    static func requestOpinions() -> [Opinion] {
        print("Send HTTP request for Opinions")
        
        //replace with actualy network requests with protocols/interfaces
        return Utility.getDummyAnswers()
    }
    
    static func insertOpinionToTopic(newOpinion: Opinion) {
        print("Inserting \(newOpinion.title) to topic")  
    }
    
    static func insertNewTopic(newTopic: Topic) {
        print("Inserting \(newTopic.title)")
    }
    
    static func insertVote(vote: Int, opinionId: String) {
        switch vote {
        case Opinion.LIKE:
            print("Insert Vote: LIKE")
        case Opinion.UNLIKE:
            print("Insert Vote: UNLIKE")
        case Opinion.NEUTRAL:
            print("Insert Vote: NEUTRAL")
        default: break
        }
    }
}