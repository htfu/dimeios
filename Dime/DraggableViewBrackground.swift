//
//  DraggableViewBrackground.swift
//  Dime
//
//  Created by Nishant Jain on 4/2/16.
//  Copyright © 2016 Dime. All rights reserved.
//

//
//  DraggableViewBackground.swift
//  TinderSwipeCardsSwift
//
//  Created by Gao Chao on 4/30/15.
//  Copyright (c) 2015 gcweb. All rights reserved.
//

import Foundation
import UIKit

class DraggableViewBackground: UIView, DraggableViewDelegate {
    var allCards: [DraggableView]!
    
    let MAX_BUFFER_SIZE = 2
    var CARD_HEIGHT: CGFloat = 386
    var CARD_WIDTH: CGFloat = 290
    
    var cardsLoadedIndex: Int!
    var loadedCards: [DraggableView]!
    var menuButton: UIButton!
    var messageButton: UIButton!
    var checkButton: UIButton!
    var xButton: UIButton!
    
    var opinionList: [Opinion]? {
        didSet {
            self.loadCards()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        super.layoutSubviews()
        CARD_HEIGHT = frame.height * 0.85
        CARD_WIDTH = frame.width * 0.9
        self.setupView()
        allCards = []
        loadedCards = []
        cardsLoadedIndex = 0
        self.loadCards()
    }
    
    func setupView() -> Void {
        //self.backgroundColor = UIColor(red: 0.92, green: 0.93, blue: 0.95, alpha: 1)
        
        //xButton = UIButton(frame: CGRectMake((self.frame.size.width - CARD_WIDTH)/2 + 35, self.frame.size.height/2 + CARD_HEIGHT/2 + 10, 59, 59))
        //xButton.setImage(UIImage(named: "xButton"), forState: UIControlState.Normal)
        //xButton.addTarget(self, action: #selector(DraggableViewBackground.swipeLeft), forControlEvents: UIControlEvents.TouchUpInside)
        
        //checkButton = UIButton(frame: CGRectMake(self.frame.size.width/2 + CARD_WIDTH/2 - 85, self.frame.size.height/2 + CARD_HEIGHT/2 + 10, 59, 59))
        //checkButton.setImage(UIImage(named: "checkButton"), forState: UIControlState.Normal)
        //checkButton.addTarget(self, action: #selector(DraggableViewBackground.swipeRight), forControlEvents: UIControlEvents.TouchUpInside)
        
        //self.addSubview(xButton)
        //self.addSubview(checkButton)
    }
    
    func createDraggableViewWithDataAtIndex(index: NSInteger) -> DraggableView {
        let draggableView = DraggableView(frame: CGRectMake((self.frame.size.width - CARD_WIDTH)/2, (self.frame.size.height - CARD_HEIGHT + 52)/2, CARD_WIDTH, CARD_HEIGHT))
        if index == -1 {
            //nil for chart card
            draggableView.opinion = nil
        } else {
            if let op = opinionList?[index] {
                draggableView.opinion = op
            }
        }
        draggableView.delegate = self
        return draggableView
    }
    
    func createDraggableViewWithChart(opinionList: [Opinion]) -> DraggableView {
        let draggableView = DraggableView(frame: CGRectMake((self.frame.size.width - CARD_WIDTH)/2, (self.frame.size.height - CARD_HEIGHT + 52)/2, CARD_WIDTH, CARD_HEIGHT), opinionList: opinionList)
        draggableView.delegate = self
        return draggableView
    }
    
    func loadCards() -> Void {
        if opinionList != nil {
            if opinionList!.count > 0 {
                let numLoadedCardsCap = opinionList!.count > MAX_BUFFER_SIZE ? MAX_BUFFER_SIZE : opinionList!.count
                for i in 0 ..< opinionList!.count {
                    let newCard: DraggableView = self.createDraggableViewWithDataAtIndex(i)
                    allCards.append(newCard)
                    if i < numLoadedCardsCap {
                        loadedCards.append(newCard)
                    }
                }
                
                for i in 0 ..< loadedCards.count {
                    if i > 0 {
                        self.insertSubview(loadedCards[i], belowSubview: loadedCards[i - 1])
                    } else {
                        self.addSubview(loadedCards[i])
                    }
                    cardsLoadedIndex = cardsLoadedIndex + 1
                }
            }
        }
        
    }
    
    func cardSwipedLeft(card: UIView) -> Void {
        removeCard()
    }
    
    func cardSwipedRight(card: UIView) -> Void {
        removeCard()
    }
    
    func cardSwipedUp(card: UIView) {
        removeCard()
    }
    
    func removeCard() {
        loadedCards.removeAtIndex(0)
        
        print("loaded cards: \(loadedCards.count)\n")
        if cardsLoadedIndex < allCards.count {
            loadedCards.append(allCards[cardsLoadedIndex])
            cardsLoadedIndex = cardsLoadedIndex + 1
            self.insertSubview(loadedCards[MAX_BUFFER_SIZE - 1], belowSubview: loadedCards[MAX_BUFFER_SIZE - 2])
        }
        
        if loadedCards.count == 0 {
            let chartCard: DraggableView = self.createDraggableViewWithChart(opinionList!)
            self.addSubview(chartCard)
        }
    }
    
    func swipeRight() -> Void {
        if loadedCards.count <= 0 {
            return
        }
        let dragView: DraggableView = loadedCards[0]
        dragView.overlayView.setMode(GGOverlayViewMode.GGOverlayViewModeRight)
        UIView.animateWithDuration(0.2, animations: {
            () -> Void in
            dragView.overlayView.alpha = 1
        })
        dragView.rightClickAction()
    }
    
    func swipeLeft() -> Void {
        if loadedCards.count <= 0 {
            return
        }
        let dragView: DraggableView = loadedCards[0]
        dragView.overlayView.setMode(GGOverlayViewMode.GGOverlayViewModeLeft)
        UIView.animateWithDuration(0.2, animations: {
            () -> Void in
            dragView.overlayView.alpha = 1
        })
        dragView.leftClickAction()
    }
}