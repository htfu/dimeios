//
//  CardViewController.swift
//  Dime
//
//  Created by Nishant Jain on 4/2/16.
//  Copyright © 2016 Dime. All rights reserved.
//

import UIKit

class CardViewController: UIViewController {
    
    var draggableBackground: DraggableViewBackground?
    var topic : Topic? {
        didSet {
            self.opinionList = topic!.opinionList
        }
    }
    var opinionList : [Opinion]? {
        didSet {
            if let db = draggableBackground {
                if db.opinionList != nil {
                    db.opinionList = opinionList
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        draggableBackground = DraggableViewBackground(frame: self.view.frame)
        draggableBackground?.opinionList = opinionList
        self.title = topic!.title
        self.view.addSubview(draggableBackground!)
    }

    @IBAction func addOpinion(sender: UIButton) {
        //Show addOpinion Dialog
        var tField: UITextField!
        
        func configurationTextField(textField: UITextField) {
            textField.placeholder = "What would you like to do"
            tField = textField
        }
        
        func handleCancel(alertView: UIAlertAction!) {
            print("Cancelled")
        }
        
        let alert = UIAlertController(title: "What is your choice?", message: "", preferredStyle: .Alert)
        alert.addTextFieldWithConfigurationHandler(configurationTextField)
        alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: handleCancel))
        alert.addAction(UIAlertAction(title: "Done", style: .Default, handler: { (UIAlertAction) in
            print("Done")
            print("New Opinion: \(tField.text)")
            if tField.text!.isEmpty == true {
                self.presentViewController(alert, animated: true, completion: {
                    print("Completed adding opinion")
                })
            }
            self.addNewOpinion(tField.text)
        }))
        
        self.presentViewController(alert, animated: true, completion: {
            print("Completed adding opinion")
        })
    }
    
    func addNewOpinion(title : String?) {
        if title != nil {
            let newOpinion = Opinion(title: title!)
            self.opinionList?.append(newOpinion)
            DimeRequestBuilder.insertOpinionToTopic(newOpinion)
            print("Total answers: \(topic!.opinionList.count)")
        }
    }
    
    // MARK: - Navigation

    /*
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    } */
    

}
