//
//  Opinion.swift
//  Dime
//
//  Created by Nishant Jain on 4/2/16.
//  Copyright © 2016 Dime. All rights reserved.
//

import Foundation

class Opinion {
    // MARK: Properties
    static let LIKE = 1
    static let UNLIKE = -1
    static let NEUTRAL = 0
    
    var title: String
    var score: Int
    var opinionId: String
    
    // MARK: Initialization
    
    init(title: String, score: Int) {
        self.title = title
        self.score = score
        self.opinionId = ""
    }
    
    init(title: String){
        self.score = 0
        self.title = title
        self.opinionId = ""
    }
}