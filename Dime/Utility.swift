//
//  Utility.swift
//  Dime
//
//  Created by Nishant Jain on 3/30/16.
//  Copyright © 2016 Dime. All rights reserved.
//

import Foundation

class Utility {
    
    
    static func getAlphaCodeFromPreferences() -> String? {
        let preferences = NSUserDefaults.standardUserDefaults()
        if let alphaCode = preferences.stringForKey(Constant.alphaKey) {
            return alphaCode
        } else {
            return nil
        }
    }
    
    static func setAlphaCodeFromPreferences(alphaCode : String) {
        let preferences = NSUserDefaults.standardUserDefaults()
        preferences.setObject(alphaCode, forKey: Constant.alphaKey)
    }
    
    static func getDummyQuestions() -> [Topic] {
        let topic1 = Topic.init(title: "What movie do we wanna watch?");
        let topic2 = Topic.init(title: "What food do we eat tonight?", round: 2, peopleVoted: 3)
        let topic3 = Topic.init(title: "Where should we have Thanksgiving?", round: 3, peopleVoted: 5)
        
        var dummyTopics = [Topic]()
        dummyTopics.append(topic1)
        dummyTopics.append(topic2)
        dummyTopics.append(topic3)
        
        return dummyTopics
    }
    
    static func getDummyAnswers() -> [Opinion] {
        let op1 = Opinion.init(title: "111")
        let op2 = Opinion.init(title: "222")
        let op3 = Opinion.init(title: "333")
        
        var dummyOpinions = [Opinion]()
        dummyOpinions.append(op1)
        dummyOpinions.append(op2)
        dummyOpinions.append(op3)
        
        return dummyOpinions
    }
}