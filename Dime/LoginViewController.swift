//
//  ViewController.swift
//  Dime
//
//  Created by Nishant Jain on 3/30/16.
//  Copyright © 2016 Dime. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var groupNameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if let oldAlphaCode = Utility.getAlphaCodeFromPreferences() {
            groupNameTextField.text = oldAlphaCode
        }
        
    }
    
    @IBAction func login() {
        if let alpha = groupNameTextField.text {
            if !DimeRequestBuilder.requestLogin(alpha) {
                groupNameTextField.text = ""
                print("Try different group name")
            } else {
                print("Successful Login")
                performSegueWithIdentifier("ShowTopics", sender: self)
            }
        } else {
            print("Enter a group name")
        }
    }
    
    @IBAction func closeKeyboard(sender: UITextField) {
        groupNameTextField.resignFirstResponder()
    }
    
}

