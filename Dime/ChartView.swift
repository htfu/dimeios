//
//  ChartView.swift
//  Dime
//
//  Created by Nishant Jain on 4/5/16.
//  Copyright © 2016 Dime. All rights reserved.
//

import Foundation
import Charts

class ChartView {
    var entries: [BarChartDataEntry] = []
    var labels: [String] = []
    var percentScores: [Float] = []
    
    func getChartView(opinionList: [Opinion], frame: CGRect) -> HorizontalBarChartView {
        
        setDataLists(opinionList)
        
        let barChartView = HorizontalBarChartView(frame: frame)
        
        let chartDataSet = BarChartDataSet(yVals: entries, label: "")
        let chartData = BarChartData(xVals: labels, dataSet: chartDataSet)
        chartDataSet.colors = ChartColorTemplates.vordiplom()
        chartDataSet.drawValuesEnabled = false
        
        
        barChartView.data = chartData
        print("Returning BarCharView with size \(chartDataSet.stackSize)")
        
        barChartView.noDataText = "Start by adding an opinion"
        barChartView.drawGridBackgroundEnabled = false
        barChartView.drawBordersEnabled = false
        barChartView.drawValueAboveBarEnabled = false
        barChartView.leftAxis.drawGridLinesEnabled = false
        barChartView.leftAxis.drawAxisLineEnabled = false
        barChartView.leftAxis.drawLabelsEnabled = false
        barChartView.rightAxis.drawGridLinesEnabled = false
        barChartView.rightAxis.drawAxisLineEnabled = false
        barChartView.rightAxis.drawLabelsEnabled = false
        barChartView.xAxis.drawGridLinesEnabled = false
        barChartView.xAxis.drawAxisLineEnabled = false
        
        barChartView.highlightPerTapEnabled = false
        barChartView.legend.enabled = false
        barChartView.descriptionText = ""
        
        return barChartView
    }
    
    func setDataLists(opinionList: [Opinion]) {
        var totalScore = getTotalScore(opinionList)
        
        /* Special Case */
        if totalScore == 0 {
            var allZero = false
            for i in 0..<opinionList.count {
                if opinionList[i].score != 0 {
                    allZero = true
                }
            }
            
            if allZero {
                totalScore = opinionList.count
                for i in 0..<opinionList.count {
                    opinionList[i].score = 1
                }
            }
        }
        
        var j = 0
        for i in 0..<opinionList.count {
            labels.append(opinionList[i].title)
            if opinionList[i].score > 0 && totalScore > 0 {
                let score: Float = Float(opinionList[i].score)
                let tsc: Float = Float(totalScore)
                let percentageValue = 100 * score / tsc
                percentScores.append(percentageValue)
                let entry = BarChartDataEntry(value: Double(percentageValue), xIndex: j)
                entries.append(entry)
                j += 1
            }
        }
    }
    
    func getTotalScore(opinionList: [Opinion]) -> Int {
        
        var min = opinionList[0].score
        var max = min
        
        if opinionList.count > 1 {
            for i in 1..<opinionList.count {
                if opinionList[i].score < min {
                    min = opinionList[i].score
                }
                if opinionList[i].score < max {
                    max = opinionList[i].score
                }
            }
        }
        
        min = abs(min)
        min += 1
        
        var totalScore = 0
        
        //Add minimum score to all opinions and to total score
        for i in 0..<opinionList.count {
            opinionList[i].score = opinionList[i].score + min
            totalScore += opinionList[i].score
        }
        
        return totalScore
    }
}