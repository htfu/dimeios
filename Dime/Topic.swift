//
//  Topic.swift
//  Dime
//
//  Created by Nishant Jain on 3/30/16.
//  Copyright © 2016 Dime. All rights reserved.
//

import Foundation

class Topic {
    // MARK: Properties
    
    var title: String
    var round: Int
    var peopleVoted: Int
    var opinionList: [Opinion]
    
    // MARK: Initialization
    
    init(title: String, round: Int, peopleVoted: Int) {
        self.title = title
        self.round = round
        self.peopleVoted = peopleVoted
        self.opinionList = Utility.getDummyAnswers()
    }
    
    init(title: String){
        self.round = 0
        self.peopleVoted = 0
        self.title = title
        self.opinionList = Utility.getDummyAnswers()
    }
}