//
//  TopicViewController.swift
//  Dime
//
//  Created by Nishant Jain on 3/30/16.
//  Copyright © 2016 Dime. All rights reserved.
//

import UIKit

class TopicViewController: UITableViewController {

    var topics: [Topic] = DimeRequestBuilder.requestTopicList()
    var selectedTopic: Topic?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return topics.count
    }

    private struct Storyboard {
        static let CellReuseIdentifier = "Topic"
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> TopicViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(Storyboard.CellReuseIdentifier, forIndexPath: indexPath) as! TopicViewCell

        // Configure the cell...
        cell.topic = topics[indexPath.row]

        return cell
    }
    
    // MARK: button responses
    @IBAction func onRefresh(sender: UIRefreshControl?) {
        print("Refreshing Questions List")
        topics = DimeRequestBuilder.requestTopicList()
        self.tableView.reloadData()
        sender?.endRefreshing()
    }
    
    @IBAction func logout(sender: UIButton) {
        print("Logging Out")
    }
    
    @IBAction func addTopic(sender: UIButton) {
        print("Add Question button pressed")
        var tField: UITextField!
        
        func configurationTextField(textField: UITextField) {
            textField.placeholder = "Ask your group a question"
            tField = textField
        }
        
        func handleCancel(alertView: UIAlertAction!) {
            print("Cancelled")
        }
        
        let alert = UIAlertController(title: "What do you wanna decide about?", message: "", preferredStyle: .Alert)
        alert.addTextFieldWithConfigurationHandler(configurationTextField)
        alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: handleCancel))
        alert.addAction(UIAlertAction(title: "Done", style: .Default, handler: { (UIAlertAction) in
            print("Done")
            print("New Topic: \(tField.text)")
            if tField.text!.isEmpty == true {
                self.presentViewController(alert, animated: true, completion: {
                    print("Completed adding Topic")
                })
            }
            self.addNewTopic(tField.text)
        }))
        
        self.presentViewController(alert, animated: true, completion: {
            print("Completed adding topic")
        })
    }
    
    func addNewTopic(title : String?) {
        if title != nil {
            let newTopic = Topic(title: title!)
            self.topics.append(newTopic)
            DimeRequestBuilder.insertNewTopic(newTopic)
            self.tableView.reloadData()
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        print("Row: \(indexPath.row)")
        selectedTopic = topics[indexPath.row]
        print("Start voting for \(selectedTopic!.title)")
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    //override func prepareForSegue(segue: UIStoryboardSegue, sender: UITableView) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
    //}
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let identifier = segue.identifier {
            if identifier == "Vote" {
                if let cardViewController = segue.destinationViewController as? CardViewController {
                    if let index = tableView.indexPathForSelectedRow?.row {
                        cardViewController.topic = topics[index]
                    }
                }
            }
        }
    }

}
