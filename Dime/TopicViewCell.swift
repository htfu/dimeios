//
//  TopicViewCell.swift
//  Dime
//
//  Created by Nishant Jain on 3/30/16.
//  Copyright © 2016 Dime. All rights reserved.
//

import UIKit

class TopicViewCell: UITableViewCell {
    
    @IBOutlet weak var topicQuestion: UILabel!
    @IBOutlet weak var topicRound: UILabel!
    @IBOutlet weak var peopleVoted: UILabel!
    
    var topic: Topic? {
        didSet {
            updateUI()
        }
    }
    
    func updateUI() {
        topicQuestion.text = topic?.title
        topicRound.text = "Round \(topic!.round)"
        peopleVoted.text = "\(topic!.peopleVoted) people voted"
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
